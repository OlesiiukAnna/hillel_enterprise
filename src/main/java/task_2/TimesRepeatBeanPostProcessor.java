package task_2;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;

@Component
public class TimesRepeatBeanPostProcessor implements BeanPostProcessor {

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        Field[] fields = bean.getClass().getDeclaredFields();
        for (Field f : fields) {
            TimesRepeat annotation = f.getAnnotation(TimesRepeat.class);
            if (annotation != null) {
                int times = annotation.value();
                System.out.println(times);
                f.setAccessible(true);
                ReflectionUtils.setField(f, bean, times);
            }
        }
        return bean;
    }
}
