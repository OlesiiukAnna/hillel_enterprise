package task_2;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericApplicationContext;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        GenericApplicationContext ctx = new AnnotationConfigApplicationContext(Configuration.class);
        InternetSurfer internetSurfer = ctx.getBean("internetSurfer", InternetSurfer.class);
        internetSurfer.pingUrl();
    }
}
