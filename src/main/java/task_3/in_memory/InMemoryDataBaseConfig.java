package task_3.in_memory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import task_3.DAO;

@Configuration
@Profile("memory")
public class InMemoryDataBaseConfig {

    @Bean
    public DAO inMemoryDAO() {
        return new InMemoryDataBaseDAO();
    }
}
