package task_3.in_memory;

import task_3.DAO;

import java.util.LinkedHashSet;
import java.util.Set;

public class InMemoryDataBaseDAO implements DAO {
    private Set<String> storage = new LinkedHashSet<>();

    @Override
    public void save(String value) {
        storage.add(value);
    }

    @Override
    public String readAll() {
        return storage.toString();
    }
}
