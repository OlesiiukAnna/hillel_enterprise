package task_3;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import task_3.file_database.FileDataBaseConfig;
import task_3.in_memory.InMemoryDataBaseConfig;

public class Main {
    public static void main(String[] args) {
        GenericApplicationContext ctx = new AnnotationConfigApplicationContext(
                InMemoryDataBaseConfig.class,
                FileDataBaseConfig.class,
                Service.class);
        Service service = ctx.getBean("service", Service.class);
        service.save("test");
        service.save("test1");
        service.save("test2");
        System.out.println(service.readAll());
        ctx.close();
    }
}
