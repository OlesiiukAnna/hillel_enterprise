package task_3;

import org.springframework.beans.factory.annotation.Autowired;

@org.springframework.stereotype.Service

public class Service {
    private DAO dao;

    @Autowired
    public Service(DAO dao) {
        this.dao = dao;
    }

    public void save(String value) {
        this.dao.save(value);
    }

    public String readAll() {
        return dao.readAll();
    }
}