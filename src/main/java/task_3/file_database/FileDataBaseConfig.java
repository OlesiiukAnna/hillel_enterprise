package task_3.file_database;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import task_3.DAO;

@Configuration
@Profile("file")
public class FileDataBaseConfig {

    @Bean
    public DAO fileDAO() {
        FileDataBaseDAO fileDataBaseDAO = new FileDataBaseDAO();
        fileDataBaseDAO.setFilePath("./test.txt");

        return fileDataBaseDAO;
    }
}
